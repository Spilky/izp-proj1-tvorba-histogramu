/*
 * Soubor:  projekt1.c
 * Datum:   17.10.2012
 * Autor:   David Spilka (xspilk00)
 * Projekt: Tvorba histogramu, projekt �. 1 pro p�edm�t IZP
 * Popis:   
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>

/* Napoveda */

void printHelp(void)
{
  printf("Napoveda k programu:\n"
       "Program: Projekt 1: Tvorba histogramu.\n"
       "Autor: David Spilka (xspilk00)\n"
       "Program pocita cetnost jednotlivych znaku, ktere jsou cteny\n"
       "ze standardniho vstupu a vypise ji v poradi danem ordinarni\n"
       "hodnotou znaku nebo v poradi danem cetnosti znaku od nejcetnejsiho\n"
       "po nejmene cetny\n"
       "Pouziti: proj1 --help\n"
       "         proj1 -h\n"
       "         proj1 -NNN (NNN = 1-256)\n"
       "Popis parametru:\n"
       "  -h               Vypise tuto obrazovku s napovedou.\n"
       "  --help           Vypise tuto obrazovku s napovedou.\n"
       "  -NNN             Vypise NNN radku, ktere odpovidaji znakum\n"
       "                   s nejvyssimi cetnostmi, a tyto radky budou\n"
       "                   serazeny od nejcasteji zastoupeneho znaku\n"
       "                   smerem k mene cetnym.Pokud ma vice znaku\n"
       "                   stejnou cetnost, tak budou vytisteny od znaku\n"
       "                   s nejmensiordinarni hodnotou smerem k nejvetsimu.\n"
       "                   NNN = 1-256.\n"
       "Priklad pouziti:\n"
       "  proj1 -34        Program vypise 34 radku, ktere odpovidaji znakum\n"
       "                   s nejvyssimi cetnostmi.\n");
  return;
}

/* Konec napovedy */

/* Na�ten� znak� */

void nacti(unsigned char pole[])
{
  int znak;
  
  while ((znak = getchar()) != EOF)
  {
    if (pole[znak] <= (UCHAR_MAX - 1))
    {
      pole[znak]++;
    }
  }
}

/* Konec na��t�n� znak� */

/* Vypis */

void vypis(int i, int hodnota)
{
     if (hodnota != 0 && hodnota <= (UCHAR_MAX - 1))
     {
        if (isprint(i))
        {
           printf("%d '%c': %d\n", i, i, hodnota);
        }
        else
        {
            printf("%d: %d\n", i, hodnota);
        }
     }
     else if (hodnota > (UCHAR_MAX - 1))
     {
        if (isprint(i))
        {
           printf("%d '%c': NNN\n", i, i);
        }
        else
        {
            printf("%d: NNN\n", i);
        }
     }
     return;
}

/* Konec vypisu */

/* Serazeni dle ordinarni hodnoty */

void dleOrdHodn(void)
{
  int i;
  unsigned char znakyc[256] = {0};
  nacti(znakyc);
  
  for(i = 0; i < 256; i++)
  {
    vypis(i, znakyc[i]);
  }
  return;
}

/* Konec serazeni dle ordinarni hodnoty */

/* Serazeni dle cetnosti */

void dleCetn(int vysl)
{
     int maxIndex;
     int j;
     int i;
     unsigned char znakyc[256] = {0};
     nacti(znakyc);
     
     for (i = 0; i < vysl; i++)
     {
         maxIndex = i;
         for (j = i + 1; j < 256; j++)
         {
             if (znakyc[j] > znakyc[maxIndex])
             {
                maxIndex = j;
             }
         }
         int tmp;
         tmp = znakyc[i];
         znakyc[i] = znakyc[maxIndex];
         znakyc[maxIndex] = tmp;
            
         vypis(maxIndex, znakyc[i]);
     }
     return;
}

/* Konec serazeni dle cetnosti */

/* Zpracovani parametru */

void doParams(int argc, char *argv[])
{
  if (argc == 1)
  {
    dleOrdHodn();
  }
  else if (argc == 2)
  {
     if ((strcmp("-h", argv[1]) == 0) || (strcmp("--help", argv[1]) == 0))
     {
     printHelp();
     }
     else
     {
        int i = 0;
        int cifra;
        int vysl = 0;
        while ( (cifra = argv[1][i]) != '\0' )
        {
          i++;
          if ((cifra >= '0') && (cifra <= '9'))
          {
           vysl *= 10;
           cifra = cifra - '0';
           vysl += cifra;
          }
          else
          {
              fprintf(stderr,"Chyba v zadani parametru!\n");
              break;
          }
        }
        
        if (vysl > 256)
        {
           fprintf(stderr,"Chyba: Pozadujete vypsat prilis mnoho prvku\n");
        }
        else
        {
            dleCetn(vysl);
        }
     }
  }
  else
  {
   fprintf(stderr,"Chyba v zadani parametru!");
  }
  return;
}

/* Konec zpracovani parametru */

/* Hlavni blok programu */

int main(int argc, char *argv[])
{
    doParams(argc, argv);
    return 0;
}

/* Konec hlavniho bloku programu */
